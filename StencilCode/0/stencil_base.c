#include <stdio.h>

#define TS 4
#define N 12
double A[N][N];

void print_matrix(double A[N][N]){
    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            printf("%7.3lf ",A[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main(int argc, char **argv){
    int counter=0;

    for(int ti=0; ti<N; ti+=TS){
        for(int tj=0; tj<N; tj+=TS){
            for(int i=ti; i<ti+TS; i++){
                for(int j=tj; j<tj+TS; j++){
                    A[i][j] = 1.1*((i*i)%37);
                }
            }
        }
    }

    print_matrix(A);

    for(int i=0; i<N; i++){
        for(int j=0; j<N; j++){
            if( i-1>=0 && j-1>=0 && i+1<N && j+1<N )
                A[i][j] = (A[i][j]+A[i-1][j]+A[i+1][j]+A[i][j-1]+A[i][j+1])/5.0;
        }
    }

    print_matrix(A);

    return 0;
}
