#include <omp.h>
#include <stdio.h>


int main(){
  #pragma omp parallel num_threads(5)
  { 
    int ID = 0;
    ID = omp_get_thread_num();
    printf("ID: %d\n", ID);
  }
}
