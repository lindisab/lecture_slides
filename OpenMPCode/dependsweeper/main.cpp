#include <omp.h>
#include <stdio.h>
#include <unistd.h>

int main(){

  int A[10][10];
  int n=10;

  #pragma omp parallel
  for(int i=0; i<n; i++){
	for(int j=0; j<n; j++){
        	A[i][j] = 0;
        }

  }

  #pragma omp parallel 
  {
    
  #pragma omp single 
  {
    // set all elements in the first row to 1
    for(int i=0; i<n; i++){
        {
		#pragma omp task depend( out: A[i][0])
		{
			A[i][0] = 1;
		}
        }
    }
    // set all elements in the first column to 1
    for(int i=0; i<n; i++){
        {
		#pragma omp task depend( out: A[0][i])
                {
			sleep(1);
                        A[0][i] = 1;
                }
        }
    }

    // update the rest of the matrix
    for(int i=1; i<n; i++){
	for(int j=1; j<n; j++){
    		#pragma omp task depend( in: A[i-1][j], A[i][j-1])  //depend( inout: A[i][j])
		{
		
      			A[i][j] = A[i-1][j] + A[i][j-1];
    		}
	}
    }


    // print filled matrix
    printf("filled matrix:\n");
    #pragma omp taskwait 
       for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
          printf("%d ", A[i][j]);
        }
        printf("\n");
       }
    
  }
  }

  printf("\n\n\nlast element:\n");
  printf("A[%d][%d]:%d\n", n, n, A[n-1][n-1]);  

  return 0;
}
